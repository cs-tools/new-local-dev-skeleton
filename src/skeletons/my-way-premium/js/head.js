$(window).on("load", function() { CreativeTemplate.WindowLoad(); });

$(function() {
    	CreativeTemplate.Init();
    });

	var CreativeTemplate = {
    	// PROPERTIES
        "KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 },
        "IsMyViewPage": false, // UPDATES IN SetTemplateProps METHOD
        "ShowDistrictHome": <SWCtrl controlname="Custom" props="Name:districtButton" />,
        "ShowSchoolList": false, // UPDATES IN SetTemplateProps METHOD
        "ShowTranslate": <SWCtrl controlname="Custom" props="Name:showTranslate" />,
		"ShowFullSiteButton": <SWCtrl controlname="Custom" props="Name:showFullSite" />,
        "ChannelBarIsSticky": false,

		// METHODS
        "Init": function() {
            // FOR SCOPE
            var _this = this;

            this.SetTemplateProps();
            this.JsMediaQueries();
			this.MyStart();
			this.SchoolList();
			this.Translate();
            this.Header();
            this.Search();
            this.ChannelBar();
            this.StickyChannelBar();
            if($(".hp").length) {
              this.Slideshow();
              this.CheckSlideshow();
            }
            this.Body();
            this.ModEvents();
            this.GlobalIcons();
            this.RsMenu();
            this.AppAccordion();
            this.Footer();
            this.SocialIcons();
            this.ViewFullSite();

            $(window).resize(function() { _this.WindowResize(); });
            $(window).scroll(function() { _this.WindowScroll(); });

            csGlobalJs.OpenInNewWindowWarning();
        },

        "SetTemplateProps": function() {
        	// MYVIEW PAGE CHECK
            if($("#pw-body").length) this.IsMyViewPage = true;

            // SCHOOL LIST CHECK
        	if($(".sw-mystart-dropdown.schoollist").length) this.ShowSchoolList = true;
        },

        "WindowLoad": function() {
			csGlobalJs.OpenInNewWindowWarning();
        },

        "WindowResize": function() {
            this.JsMediaQueries();
            this.StickyChannelBar();
            if($(".hp").length) {
                this.CheckSlideshow();
            }
        },

        "WindowScroll": function() {
            this.StickyChannelBar();
            if($(".hp").length) {
                this.CheckSlideshow();
            }
        },

        "JsMediaQueries": function() {
            switch(this.GetBreakPoint()) {
                case "desktop":

                break;
                case "768":

                break;
                case "640":

                break;
                case "480":

                break;
                case "320":

                break;
            }
        },

        "MyStart": function() {
        	// FOR SCOPE
            var _this = this;

			// ADD DISTRICT HOME
            if(this.ShowDistrictHome && $(".sw-mystart-button.home").length) {
            	$(".sw-mystart-button.home > a > span").text("<SWCtrl controlname="Custom" props="Name:districtHomeText" />");
            	$(".gb-mystart.left").prepend($(".sw-mystart-button.home"));
            }

            // BUILD USER OPTIONS DROPDOWN
            var userOptionsItems = "";

            [$IF LOGGED IN$]

			// MYVIEW BUTTON
			if($(".sw-mystart-button.pw").length) {
                userOptionsItems += '<li><a href="' + window.location.protocol + '//' + window.location.host + '/myview"><span>MyView</span></a></li>';
            }

			// SITE MANAGER BUTTON
            if($(".sw-mystart-button.manage").length) {
            	$(".sw-mystart-button.manage a").attr("onclick", $(".sw-mystart-button.manage a").attr("onclick") + "return false;");
                userOptionsItems += '<li id="user-options-manage"><a href="#" onclick="' + $(".sw-mystart-button.manage a").attr("onclick") + '"><span>Site Manager</span></a></li>';
            }

			// MY ACCOUNT BUTTONS
            $("#sw-myaccount-list > li:first-child a span").text("My Account");
			$("#sw-myaccount-list > li").each(function() {
				userOptionsItems += "<li>" + $(this).html() + "</li>";
			});

			// MY PASSKEYS BUTTON
			if($("#sw-mystart-mypasskey").length) {
                $("#sw-mystart-mypasskey a span").text("Passkeys");
                $(".gb-mystart.right").prepend($("#sw-mystart-mypasskey"));
            }

            [$ELSE IF LOGGED$]

			// SIGNIN BUTTON
            if($(".sw-mystart-button.signin").length) {
				userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
			}

			// REGISTER BUTTON
            if($(".sw-mystart-button.register").length) {
                userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
            }

            [$END IF LOGGED IN$]

			// ADD USER OPTIONS DROPDOWN TO THE DOM
            $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems);

			// BIND DROPDOWN EVENTS
			this.DropdownActions({
            	"dropdownParent": ".cs-mystart-dropdown.user-options",
                "dropdownSelector": ".cs-selector",
                "dropdown": ".cs-dropdown",
                "dropdownList": ".cs-dropdown-list"
            });
        },

		"SchoolList": function() {
			// ADD SCHOOL LIST
			if(this.ShowSchoolList) {
				var schoolDropdown =	'<div class="cs-mystart-dropdown schools">' +
											'<div class="cs-selector" tabindex="0" aria-label="<SWCtrl controlname="Custom" props="Name:schoolDropdownText" />" role="button" aria-expanded="false" aria-haspopup="true"><SWCtrl controlname="Custom" props="Name:schoolDropdownText" /></div>' +
											'<div class="cs-dropdown" aria-hidden="true" style="display:none;">' +
												'<ul class="cs-dropdown-list">' + $(".sw-mystart-dropdown.schoollist .sw-dropdown-list").html() + '</ul>' +
											'</div>' +
										'</div>';

				// ADD SCHOOL LIST TO THE DOM
				$(".gb-mystart.left").append(schoolDropdown);

				// BIND DROPDOWN EVENTS
				this.DropdownActions({
					"dropdownParent": ".cs-mystart-dropdown.schools",
					"dropdownSelector": ".cs-selector",
					"dropdown": ".cs-dropdown",
					"dropdownList": ".cs-dropdown-list"
				});
			}
		},

		"Translate": function() {
			// ADD TRANSLATE
            if(this.ShowTranslate) {
                $(".cs-mystart-dropdown.translate .cs-dropdown").creativeTranslate({
                    "type": 2, // 1 = FRAMESET, 2 = BRANDED, 3 = API
					"languages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
						["Afrikaans", "Afrikaans", "af"],
						["Albanian", "shqiptar", "sq"],
						["Amharic", "አማርኛ", "am"],
						["Arabic", "العربية", "ar"],
						["Armenian", "հայերեն", "hy"],
						["Azerbaijani", "Azərbaycan", "az"],
						["Basque", "Euskal", "eu"],
						["Belarusian", "Беларуская", "be"],
						["Bengali", "বাঙালি", "bn"],
						["Bosnian", "bosanski", "bs"],
						["Bulgarian", "български", "bg"],
						["Burmese", "မြန်မာ", "my"],
						["Catalan", "català", "ca"],
						["Cebuano", "Cebuano", "ceb"],
						["Chichewa", "Chichewa", "ny"],
						["Chinese Simplified", "简体中文", "zh-CN"],
						["Chinese Traditional", "中國傳統的", "zh-TW"],
						["Corsican", "Corsu", "co"],
						["Croatian", "hrvatski", "hr"],
						["Czech", "čeština", "cs"],
						["Danish", "dansk", "da"],
						["Dutch", "Nederlands", "nl"],
						["Esperanto", "esperanto", "eo"],
						["Estonian", "eesti", "et"],
						["Filipino", "Pilipino", "tl"],
						["Finnish", "suomalainen", "fi"],
						["French", "français", "fr"],
						["Galician", "galego", "gl"],
						["Georgian", "ქართული", "ka"],
						["German", "Deutsche", "de"],
						["Greek", "ελληνικά", "el"],
						["Gujarati", "ગુજરાતી", "gu"],
						["Haitian Creole", "kreyòl ayisyen", "ht"],
						["Hausa", "Hausa", "ha"],
						["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
						["Hebrew", "עִברִית", "iw"],
						["Hindi", "हिंदी", "hi"],
						["Hmong", "Hmong", "hmn"],
						["Hungarian", "Magyar", "hu"],
						["Icelandic", "Íslenska", "is"],
						["Igbo", "Igbo", "ig"],
						["Indonesian", "bahasa Indonesia", "id"],
						["Irish", "Gaeilge", "ga"],
						["Italian", "italiano", "it"],
						["Japanese", "日本語", "ja"],
						["Javanese", "Jawa", "jw"],
						["Kannada", "ಕನ್ನಡ", "kn"],
						["Kazakh", "Қазақ", "kk"],
						["Khmer", "ភាសាខ្មែរ", "km"],
						["Korean", "한국어", "ko"],
						["Kurdish", "Kurdî", "ku"],
						["Kyrgyz", "Кыргызча", "ky"],
						["Lao", "ລາວ", "lo"],
						["Latin", "Latinae", "la"],
						["Latvian", "Latvijas", "lv"],
						["Lithuanian", "Lietuvos", "lt"],
						["Luxembourgish", "lëtzebuergesch", "lb"],
						["Macedonian", "Македонски", "mk"],
						["Malagasy", "Malagasy", "mg"],
						["Malay", "Malay", "ms"],
						["Malayalam", "മലയാളം", "ml"],
						["Maltese", "Malti", "mt"],
						["Maori", "Maori", "mi"],
						["Marathi", "मराठी", "mr"],
						["Mongolian", "Монгол", "mn"],
						["Myanmar", "မြန်မာ", "my"],
						["Nepali", "नेपाली", "ne"],
						["Norwegian", "norsk", "no"],
						["Nyanja", "madambwe", "ny"],
						["Pashto", "پښتو", "ps"],
						["Persian", "فارسی", "fa"],
						["Polish", "Polskie", "pl"],
						["Portuguese", "português", "pt"],
						["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
						["Romanian", "Română", "ro"],
						["Russian", "русский", "ru"],
						["Samoan", "Samoa", "sm"],
						["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
						["Serbian", "Српски", "sr"],
						["Sesotho", "Sesotho", "st"],
						["Shona", "Shona", "sn"],
						["Sindhi", "سنڌي", "sd"],
						["Sinhala", "සිංහල", "si"],
						["Slovak", "slovenský", "sk"],
						["Slovenian", "slovenski", "sl"],
						["Somali", "Soomaali", "so"],
						["Spanish", "Español", "es"],
						["Sundanese", "Sunda", "su"],
						["Swahili", "Kiswahili", "sw"],
						["Swedish", "svenska", "sv"],
						["Tajik", "Тоҷикистон", "tg"],
						["Tamil", "தமிழ்", "ta"],
						["Telugu", "తెలుగు", "te"],
						["Thai", "ไทย", "th"],
						["Turkish", "Türk", "tr"],
						["Ukrainian", "український", "uk"],
						["Urdu", "اردو", "ur"],
						["Uzbek", "O'zbekiston", "uz"],
						["Vietnamese", "Tiếng Việt", "vi"],
						["Welsh", "Cymraeg", "cy"],
						["Western Frisian", "Western Frysk", "fy"],
						["Xhosa", "isiXhosa", "xh"],
						["Yiddish", "ייִדיש", "yi"],
						["Yoruba", "yorùbá", "yo"],
						["Zulu", "Zulu", "zu"]
					],
					"advancedOptions": {
						"addMethod": "append", // PREPEND OR APPEND THE TRANSLATE ELEMENT
						"dropdownHandleText": "<SWCtrl controlname="Custom" props="Name:translateDropdownText" />", // ONLY FOR FRAMESET AND API VERSIONS AND NOT USING A CUSTOM ELEMENT
						"customElement": { // ONLY FOR FRAMESET AND API VERSIONS
							"useCustomElement": false,
							"translateItemsList": true, // true = THE TRANSLATE ITEMS WILL BE AN UNORDERED LIST, false = THE TRANSLATE ITEMS WILL JUST BE A COLLECTION OF <a> TAGS
							"customElementMarkup": "" // CUSTOM HTML MARKUP THAT MAKES THE CUSTOM TRANSLATE ELEMENT/STRUCTURE - USE [$CreativeTranslateListItems$] ACTIVE BLOCK IN THE MARKUP WHERE THE TRANSLATE ITEMS SHOULD BE ADDED
						},
						"apiKey": "", // ONLY FOR API VERSION
						"brandedLayout": 1, // 1 = VERTICAL (TEXT UNDER SELECT), 2 = HORIZONTAL (TEXT BESIDE SELECT), 3 = NON-RESPONSIVE MEGA DROPDOWN
						"removeBrandedDefaultStyling": true
                    },
					"translateLoaded": function() {}
                });
            }
		},

		"DropdownActions": function(params) {
        	// FOR SCOPE
            var template = this;

            var dropdownParent = params.dropdownParent;
            var dropdownSelector = params.dropdownSelector;
            var dropdown = params.dropdown;
            var dropdownList = params.dropdownList;

            $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");

            // MYSTART DROPDOWN SELECTOR CLICK EVENT
            $(dropdownParent).on("click", dropdownSelector, function(e) {
                e.preventDefault();

                if($(this).parent().hasClass("open")){
                	$("+ " + dropdownList + " a").attr("tabindex", "-1");
                    $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                } else {
                	$(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
                }
        	});

            // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownSelector, function(e) {
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.enter:
                    case template.KeyCodes.space:
                        e.preventDefault();

                        // IF THE DROPDOWN IS OPEN, CLOSE IT
                        if($(dropdownParent).hasClass("open")){
                        	$("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        } else {
                        	$(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
                                $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                            });
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if($("+ " + dropdown + " " + dropdownList + " a").length) {
                        	$("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        	$(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        }
                    break;

                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.down:
                    case template.KeyCodes.right:
                        e.preventDefault();

                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
                    break;
                }
            });

            // MYSTART DROPDOWN LINK KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownList + " li a", function(e) {
                // CAPTURE KEY CODE
                switch(e.keyCode) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.left:
                    case template.KeyCodes.up:
                        e.preventDefault();

                        // IS FIRST ITEM
                        if($(this).parent().is(":first-child")) {
                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        } else {
                            // FOCUS PREVIOUS ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME RIGHT AND DOWN ARROWS
                    case template.KeyCodes.right:
                    case template.KeyCodes.down:
                        e.preventDefault();

                        // IS LAST ITEM
                        if($(this).parent().is(":last-child")) {
                            // FOCUS FIRST ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                        } else {
                            // FOCUS NEXT ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                    	if(e.shiftKey) {
                        	e.preventDefault();

                        	// FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        }
                    break;

                    // CONSUME HOME KEY
                    case template.KeyCodes.home:
                        e.preventDefault();

                        // FOCUS FIRST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME END KEY
                    case template.KeyCodes.end:
                        e.preventDefault();

                        // FOCUS LAST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME ESC KEY
                    case template.KeyCodes.esc:
                    	e.preventDefault();

                        // FOCUS DROPDOWN BUTTON
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownParent).find(dropdownSelector).focus().attr("aria-expanded","false");
                        $(this).closest(dropdownParent).removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                   break;
                }
            });

            var dropdownTimer;
            $(dropdownParent).mouseenter(function() {
            	clearTimeout(dropdownTimer);
            }).mouseleave(function() {
            	var thisDropdownParent = this;

				dropdownTimer = setTimeout(function(){
                	if($(thisDropdownParent).find("#google_translate_element").length) {
                    	if(!$(thisDropdownParent).find("#cs-branded-translate-label").is(":focus")){
                        	$(dropdownList + " a", this).attr("tabindex", "-1");
                            $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        } else {
                        	$("#cs-branded-translate-label").on("change", function() {
								$(".goog-logo-link").focus();
                            });
                        }
                    } else {
                		$(dropdownList + " a", thisDropdownParent).attr("tabindex", "-1");
						$(dropdownSelector, thisDropdownParent).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            		}
                }, 500);
            }).focusout(function() {
                var thisDropdownParent = this;

                setTimeout(function () {
                    if(!$(thisDropdownParent).find(":focus").length && !$(thisDropdownParent).find("#google_translate_element").length) {
                        $(dropdownSelector, thisDropdownParent).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    }
                }, 500);
            });

            $(".cs-mystart-dropdown:not(.translate) .cs-selector").focus(function() {
            	$(".cs-mystart-dropdown.translate .cs-selector").attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            });
            $(".sw-mystart-button > a").focus(function() {
            	$(".cs-mystart-dropdown.translate .cs-selector").attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            });
        },

		"Header": function() {
            // ADD LOGO
            var logoSrc = jQuery.trim("<SWCtrl controlname="Custom" props="Name:schoolLogo" />");
			var srcSplit = logoSrc.split("/");
			var srcSplitLen = srcSplit.length;
			if((logoSrc != "") && (srcSplit[srcSplitLen - 1] != "default-man.jpg")) {
            	if(!<SWCtrl controlname="Custom" props="Name:showSchoolName" />) {
                	$("#gb-logo").append("<h1><a href='/[$SITEALIAS$]'><img src='" + logoSrc + "' alt='[$SiteName$] Logo' /></a></h1>").removeClass("hidden");
                } else {
					$("#gb-logo").append("<a href='/[$SITEALIAS$]'><img src='" + logoSrc + "' alt='[$SiteName$] Logo' /></a>").removeClass("hidden");
                }
			} else {
            	if(!<SWCtrl controlname="Custom" props="Name:showSchoolName" />) {
                	$("#gb-logo").append("<h1><a href='/[$SITEALIAS$]'><img src='/cms/lib/SWCS000001/Centricity/Template/119/defaults/logo.png' alt='[$SiteName$] Logo' /></a></h1>").removeClass("hidden");
                } else {
					$("#gb-logo").append("<a href='/[$SITEALIAS$]'><img src='/cms/lib/SWCS000001/Centricity/Template/119/defaults/logo.png' alt='[$SiteName$] Logo' /></a>").removeClass("hidden");
                }
			}

            if("<SWCtrl controlname="Custom" props="Name:schoolNameText" />" == "") {
            	$("#gb-sitename h1").remove();
            }
        },

        "ChannelBar": function() {
            $(".sw-channel-item").unbind('hover');
            $(".sw-channel-item").hover(function(){
                $(".sw-channel-item ul").stop(true, true);
                var subList = $(this).children('ul');
                if ($.trim(subList.html()) !== "") {
                    subList.slideDown(300, "swing");
                }
                $(this).addClass("hover");
            }, function(){
                $(".sw-channel-dropdown").slideUp(300, "swing");
                $(this).removeClass("hover");
            });

            $(".sw-channel-item").each(function(){
            	if($(".sw-channel-dropdown li", this).length == 0) {
                	$(this).addClass("no-dropdown");
                }
            });
        },

        "StickyChannelBar": function() {
			if(!this.IsMyViewPage) {
                var navOffSet = $("nav").offset().top;
                if ($(window).scrollTop() <= navOffSet) {
                    if(this.ChannelBarIsSticky) {
                        $("nav").removeClass("sticky").removeAttr("style");

                        this.ChannelBarIsSticky = false;
                    }
                } else {
                    $("nav").css("height", $("#gb-channel-list-outer").outerHeight(true));

                    if(!this.ChannelBarIsSticky) {
                        $("nav").addClass("sticky");

                        this.ChannelBarIsSticky = true;
                    }

                    if($(".ui-widget.app.calendar").length) {
                        if($(".wcm-controls").hasClass("wcm-stuck")) {
                            $(".wcm-controls").css("margin-top", $("#gb-channel-list-outer").outerHeight(true));
                        }
                    }
                }

                $("#sw-maincontent").css({
                    "display": "block",
                    "position": "relative",
                    "top": "-" + $("#gb-channel-list-outer").outerHeight(true) + "px"
                });
            }
        },

        "Body": function() {
        	// FOR SCOPE
            var _this = this;

            // ADD CLASS TO NAV IF DASHBOARD IS ACTIVE
            if($("#dashboard-sidebar").length) {
                $("nav").addClass("dashboard-active");
            }

            // AUTO FOCUS SIGN IN FIELD
            $("#swsignin-txt-username").focus();

            // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
            $(".ui-widget.app .ui-widget-detail img")
                .not($(".ui-widget.app.multimedia-gallery .ui-widget-detail img"))
                .each(function() {
                    if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                        $(this).css({"width": "100%", "max-width": $(this).attr("width") + "px", "height": "auto", "max-height": $(this).attr("height") + "px"});
                    }
            });

            // ADJUST FIRST BREADCRUMB
            $("li.ui-breadcrumb-first > a > span").text("Home");

            // SUBPAGE
            // USE CHANNEL NAME FOR PAGE NAV HEADER IF ONE DOESN'T EXIST
            if(!$("div.sp-column.one .ui-widget-header h1").length > 0) {
                $("div.sp-column.one .ui-widget-header").append("<h1>[$ChannelName$]</h1>");
            }

            // APP FOOTER BTNS
            $(".ui-widget.app").each(function() {
            	var thisApp = this;
                $(".ui-widget-footer",thisApp).find(".clear").remove();

                $(thisApp).find(".ui-widget-footer").addClass("displayFlexRow");

                if($(".ui-read-more",thisApp).length) {
                	$(thisApp).find(".ui-read-more").prependTo($(thisApp).find(".ui-widget-footer"));
                }
            });
            $(".ui-widget.app.navigation").each(function() {
            	var thisShortcuts = this;

                if($(".app-level-social-follow",thisShortcuts).length) {
                	$(thisShortcuts).find(".app-level-social-follow").prependTo($(thisShortcuts).find(".ui-widget-footer"));
                }
            });
        },

        "Footer": function() {
        	// FOR SCOPE
            var _this = this;

            // MOVE Bb FOOTER STUFF
            $(".gb-schoolwires-footer.logo").html($("#sw-footer-logo").html());
            var schoolwiresLinks = '';
            schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(0)").html().replace("|", "")) + '</li>';
            schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(2)").html().replace("|", "")) + '</li>';
            schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(1)").html().replace("|", "")) + '</li>';
            $(".gb-schoolwires-footer.links").append('<ul>' + schoolwiresLinks + '</ul>');
            $(".gb-schoolwires-footer.copyright").append($("#sw-footer-copyright").html());

            // FOOTER LINKS
            var footerLinks = [
				{
					"text": "<SWCtrl controlname="Custom" props="Name:footerLink1Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:footerLink1Url" />",
					"target": "<SWCtrl controlname="Custom" props="Name:footerLink1Target" />"
				},
				{
					"text": "<SWCtrl controlname="Custom" props="Name:footerLink2Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:footerLink2Url" />",
					"target": "<SWCtrl controlname="Custom" props="Name:footerLink2Target" />"
				},
				{
					"text": "<SWCtrl controlname="Custom" props="Name:footerLink3Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:footerLink3Url" />",
					"target": "<SWCtrl controlname="Custom" props="Name:footerLink3Target" />"
				},
				{
					"text": "<SWCtrl controlname="Custom" props="Name:footerLink4Text" />",
					"url": "<SWCtrl controlname="Custom" props="Name:footerLink4Url" />",
					"target": "<SWCtrl controlname="Custom" props="Name:footerLink4Target" />"
				}
			];

			var links = '';
			$.each(footerLinks, function(index, link) {
				if(link.text != "") {
					links += '<li><a href="' + link.url + '" target="' + link.target + '"><span>' + link.text + '</span></a></li>';
				}
			});

			if(links.length) {
				$("#gb-footer-links").html('<ul>' + links + '</ul>');
			} else {
            	$("#gb-footer-links").remove();
            }
        },

        "Slideshow": function() {
            // FOR SCOPE
            var _this = this;

            if($("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery").length) {
                this.MMGPlugin();
            }

            // MMG PLACEHOLDER
            if($(".sw-special-mode-bar").length && !$("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery").length) {
                $.csMultimediaGalleryDefault({
                    "contentRegion" : "#sw-content-container10.ui-hp",
                    "defaultImageSrc" : "/cms/lib/SWCS000001/Centricity/template/119/defaults/slideshow-default.jpg",
                    "allLoaded": function() {
                        _this.MMGPlugin();
                    }
                });
            }
        },

        "MMGPlugin": function() {
            // FOR SCOPE
            var _this = this;
            var clickablePhoto;

            if(<SWCtrl controlname="Custom" props="Name:clickableMMGPhoto" />) {
            	clickablePhoto = "['image']";
            } else {
            	clickablePhoto = "[]";
            }

            var mmg = eval("multimediaGallery" + $("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
            mmg.props.defaultGallery = false;

            $("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                "efficientLoad" : true,
                "imageWidth" : 1440,
                "imageHeight" : 900,
                "mobileDescriptionContainer": [640, 480, 320], // [960, 768, 640, 480, 320]
                "galleryOverlay" : false,
                "linkedElement" : clickablePhoto,  // ["image", "title", "overlay"]
                "playPauseControl" : true,
                "backNextControls" : true,
                "bullets" : false,
                "thumbnails" : false,
                "thumbnailViewerNum": [3, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                "autoRotate" : true,
                "hoverPause" : true,
                "transitionType" : "fade",
                "transitionSpeed" : <SWCtrl controlname="Custom" props="Name:transitionSpeed" />,
                "transitionDelay" : <SWCtrl controlname="Custom" props="Name:transitionDelay" />,
                "fullScreenRotator" : false,
                "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onTransitionStart" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.nextRecordIndex, props.nextGalleryIndex, props.mmgRecords
                "onTransitionEnd" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                "allLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onWindowResize": function(props) {} // props.element, props.mmgRecords
            });
        },

        "CheckSlideshow": function() {
        	// FOR SCOPE
            var _this = this;

            if($(".hp").length && this.GetBreakPoint() != "desktop") {
                if ($(window).scrollTop() <= $("#hp-slideshow").offset().top + $("#hp-slideshow").height()) {
                    if(this.SlideshowDescFixed) {
                        $("#hp-slideshow-outer").removeAttr("style");

                        this.SlideshowDescFixed = false;
                    }
                } else {
                    if(!this.SlideshowDescFixed) {
                        $("#hp-slideshow-outer").css({
                            "height": $("#hp-slideshow-outer").height(),
                            "overflow": "hidden"
                        });

                        this.SlideshowDescFixed = true;
                    }
                }
            } else {
            	$("#hp-slideshow-outer").removeAttr("style");
            }
        },

        "ModEvents": function() {
            $(".ui-widget.app.upcomingevents").modEvents({
				columns     : "yes",
				monthLong   : "no",
				dayWeek     : "yes"
			});

			$(".ui-widget.app.upcomingevents .ui-article").each(function() {
				if (!$("h1.ui-article-title.sw-calendar-block-date", this).length){
					var moveArticle = $(this).html();
					$(this).parent().prev("li").find(".upcoming-column.right").append(moveArticle);
					$(this).parent().remove();
				}
			});

            $(".upcomingevents .ui-article-description").each(function() {
                var thisDesc = this;
                if(!$(thisDesc).find(".sw-calendar-block-time").length) {
                    $('<span class="sw-calendar-block-time">All Day</span>').appendTo(thisDesc);
                } else {
                    $(thisDesc).find(".sw-calendar-block-time").appendTo(thisDesc);
                }
            });

            $('.upcomingevents').each(function() {
               if(!$('.ui-article', this).length) {
                   $('.ui-articles',this).html('<li class="no-events"><div class="ui-article"><div class="upcoming-column left"></div><div class="upcoming-column right"><p class="ui-article-description">There are no upcoming events to display.</p></div></div></li>');
               }
            });
        },

        "GlobalIcons": function() {
            $("#gb-global-icons").creativeIcons({
				"iconNum"       : "<SWCtrl controlname="Custom" props="Name:NumOfIcons" />",
				"defaultIconSrc": "",
				"icons"         : [
					{
						"image": "<SWCtrl controlname="Custom" props="Name:Icon1Image" />",
						"showText": true,
						"text": "<SWCtrl controlname="Custom" props="Name:Icon1Text" />",
						"url": "<SWCtrl controlname="Custom" props="Name:Icon1Link" />",
						"target": "<SWCtrl controlname="Custom" props="Name:Icon1Target" />"
					},
					{
						"image": "<SWCtrl controlname="Custom" props="Name:Icon2Image" />",
						"showText": true,
						"text": "<SWCtrl controlname="Custom" props="Name:Icon2Text" />",
						"url": "<SWCtrl controlname="Custom" props="Name:Icon2Link" />",
						"target": "<SWCtrl controlname="Custom" props="Name:Icon2Target" />"
					},
					{
						"image": "<SWCtrl controlname="Custom" props="Name:Icon3Image" />",
						"showText": true,
						"text": "<SWCtrl controlname="Custom" props="Name:Icon3Text" />",
						"url": "<SWCtrl controlname="Custom" props="Name:Icon3Link" />",
						"target": "<SWCtrl controlname="Custom" props="Name:Icon3Target" />"
					},
					{
						"image": "<SWCtrl controlname="Custom" props="Name:Icon4Image" />",
						"showText": true,
						"text": "<SWCtrl controlname="Custom" props="Name:Icon4Text" />",
						"url": "<SWCtrl controlname="Custom" props="Name:Icon4Link" />",
						"target": "<SWCtrl controlname="Custom" props="Name:Icon4Target" />"
					},
					{
						"image": "<SWCtrl controlname="Custom" props="Name:Icon5Image" />",
						"showText": true,
						"text": "<SWCtrl controlname="Custom" props="Name:Icon5Text" />",
						"url": "<SWCtrl controlname="Custom" props="Name:Icon5Link" />",
						"target": "<SWCtrl controlname="Custom" props="Name:Icon5Target" />"
					}
				],
				"siteID"        : "[$SiteID$]",
				"siteAlias"     : "[$SiteAlias$]",
				"calendarLink"  : "[$SiteCalendarLink$]",
				"contactEmail"  : "[$SiteContactEmail$]",
				"allLoaded"     : function(){ }
			});
        },

		"SocialIcons": function() {
			var socialIcons = [
				{
					"show": <SWCtrl controlname="Custom" props="Name:showFacebook" />,
					"label": "Facebook",
					"class": "facebook",
					"url": "<SWCtrl controlname="Custom" props="Name:FacebookUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:FacebookTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showTwitter" />,
					"label": "Twitter",
					"class": "twitter",
					"url": "<SWCtrl controlname="Custom" props="Name:TwitterUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:TwitterTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showYouTube" />,
					"label": "YouTube",
					"class": "youtube",
					"url": "<SWCtrl controlname="Custom" props="Name:YouTubeUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:YouTubeTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showInstagram" />,
					"label": "Instagram",
					"class": "instagram",
					"url": "<SWCtrl controlname="Custom" props="Name:InstagramUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:InstagramTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showLinkedIn" />,
					"label": "LinkedIn",
					"class": "linkedin",
					"url": "<SWCtrl controlname="Custom" props="Name:LinkedInUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:LinkedInTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showPinterest" />,
					"label": "Pinterest",
					"class": "pinterest",
					"url": "<SWCtrl controlname="Custom" props="Name:PinterestUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:PinterestTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showFlickr" />,
					"label": "Flickr",
					"class": "flickr",
					"url": "<SWCtrl controlname="Custom" props="Name:FlickrUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:FlickrTarget" />"
				},
                {
					"show": <SWCtrl controlname="Custom" props="Name:showPeachjar" />,
					"label": "Peachjar",
					"class": "peachjar",
					"url": "<SWCtrl controlname="Custom" props="Name:peachjarUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:peachjarTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showVimeo" />,
					"label": "Vimeo",
					"class": "vimeo",
					"url": "<SWCtrl controlname="Custom" props="Name:VimeoUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:VimeoTarget" />"
				},
				{
					"show": <SWCtrl controlname="Custom" props="Name:showRss" />,
					"label": "RSS",
					"class": "rss",
					"url": "<SWCtrl controlname="Custom" props="Name:RssUrl" />",
					"target": "<SWCtrl controlname="Custom" props="Name:RssTarget" />"
				}
			];

			var icons = '';
			$.each(socialIcons, function(index, icon) {
				if(icon.show) {
					icons += '<li><a class="gb-social-media-icon ' + icon.class + '" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a></li>';
				}
			});

			if(icons.length) {
				$("#gb-social-media-icons").html('<ul>' + icons + '</ul>').removeClass("hidden");
			}
		},

        "RsMenu": function() {
			// FOR SCOPE
			var _this = this;

            // GLOBAL ICONS
            var headerLinks = [
            	{
                    "text": "<SWCtrl controlname="Custom" props="Name:Icon1Text" />",
                    "url": "<SWCtrl controlname="Custom" props="Name:Icon1Link" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:Icon1Target" />"
                },
                {
                    "text": "<SWCtrl controlname="Custom" props="Name:Icon2Text" />",
                    "url": "<SWCtrl controlname="Custom" props="Name:Icon2Link" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:Icon2Target" />"
                },
                {
                    "text": "<SWCtrl controlname="Custom" props="Name:Icon3Text" />",
                    "url": "<SWCtrl controlname="Custom" props="Name:Icon3Link" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:Icon3Target" />"
                },
                {
                    "text": "<SWCtrl controlname="Custom" props="Name:Icon4Text" />",
                    "url": "<SWCtrl controlname="Custom" props="Name:Icon4Link" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:Icon4Target" />"
                },
                {
                    "text": "<SWCtrl controlname="Custom" props="Name:Icon5Text" />",
                    "url": "<SWCtrl controlname="Custom" props="Name:Icon5Link" />",
                    "target": "<SWCtrl controlname="Custom" props="Name:Icon5Target" />"
                }
            ];

            var haveLinks = false;
            var links = [];
            $.each(headerLinks, function(index, link) {
            	if(link.text != "") {
                	links.push({ "text": link.text, "url": link.url, "target": link.target });

                    haveLinks = true;
                }
            });

            var additionalItems = {};
            if(haveLinks) {
            	additionalItems["Popular Links"] = links;
            }

            $.csRsMenu({
                "breakPoint" : 768, // SYSTEM BREAK POINTS - 768, 640, 480, 320
                "slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
                "menuButtonParent" : "#gb-channel-list",
                "menuBtnText" : "Menu",
                "colors": {
                    "pageOverlay": "<SWCtrl controlname="Custom" props="Name:rsMenuPageOverlayBackgroundColor" />", // DEFAULT #000000
                    "menuBackground": "<SWCtrl controlname="Custom" props="Name:rsMenuBackgroundColor" />", // DEFAULT #FFFFFF
                    "menuText": "<SWCtrl controlname="Custom" props="Name:rsMenuTextColor" />", // DEFAULT #333333
                    "menuTextAccent": "<SWCtrl controlname="Custom" props="Name:rsMenuTextAccentColor" />", // DEFAULT #333333
                    "dividerLines": "<SWCtrl controlname="Custom" props="Name:rsMenuDividerLinesColor" />", // DEFAULT #E6E6E6
                    "buttonBackground": "<SWCtrl controlname="Custom" props="Name:rsMenuButtonBackgroundColor" />", // DEFAULT #E6E6E6
                    "buttonText": "<SWCtrl controlname="Custom" props="Name:rsMenuButtonTextColor" />" // DEFAULT #333333
                },
                "showDistrictHome": _this.ShowDistrictHome,
				"districtHomeText": "<SWCtrl controlname="Custom" props="Name:districtHomeText" />",
                "showSchools" : _this.ShowSchoolList,
                "schoolMenuText": "<SWCtrl controlname="Custom" props="Name:schoolDropdownText" />",
                "showTranslate" : _this.ShowTranslate,
                "translateMenuText": "<SWCtrl controlname="Custom" props="Name:translateDropdownText" />",
                "translateVersion": 2, // 1 = FRAMESET, 2 = BRANDED
                "translateId" : "",
                "translateLanguages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
                    ["Afrikaans", "Afrikaans", "af"],
                    ["Albanian", "shqiptar", "sq"],
                    ["Amharic", "አማርኛ", "am"],
                    ["Arabic", "العربية", "ar"],
                    ["Armenian", "հայերեն", "hy"],
                    ["Azerbaijani", "Azərbaycan", "az"],
                    ["Basque", "Euskal", "eu"],
                    ["Belarusian", "Беларуская", "be"],
                    ["Bengali", "বাঙালি", "bn"],
                    ["Bosnian", "bosanski", "bs"],
                    ["Bulgarian", "български", "bg"],
                    ["Burmese", "မြန်မာ", "my"],
                    ["Catalan", "català", "ca"],
                    ["Cebuano", "Cebuano", "ceb"],
                    ["Chichewa", "Chichewa", "ny"],
                    ["Chinese Simplified", "简体中文", "zh-CN"],
                    ["Chinese Traditional", "中國傳統的", "zh-TW"],
                    ["Corsican", "Corsu", "co"],
                    ["Croatian", "hrvatski", "hr"],
                    ["Czech", "čeština", "cs"],
                    ["Danish", "dansk", "da"],
                    ["Dutch", "Nederlands", "nl"],
                    ["Esperanto", "esperanto", "eo"],
                    ["Estonian", "eesti", "et"],
                    ["Filipino", "Pilipino", "tl"],
                    ["Finnish", "suomalainen", "fi"],
                    ["French", "français", "fr"],
                    ["Galician", "galego", "gl"],
                    ["Georgian", "ქართული", "ka"],
                    ["German", "Deutsche", "de"],
                    ["Greek", "ελληνικά", "el"],
                    ["Gujarati", "ગુજરાતી", "gu"],
                    ["Haitian Creole", "kreyòl ayisyen", "ht"],
                    ["Hausa", "Hausa", "ha"],
                    ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
                    ["Hebrew", "עִברִית", "iw"],
                    ["Hindi", "हिंदी", "hi"],
                    ["Hmong", "Hmong", "hmn"],
                    ["Hungarian", "Magyar", "hu"],
                    ["Icelandic", "Íslenska", "is"],
                    ["Igbo", "Igbo", "ig"],
                    ["Indonesian", "bahasa Indonesia", "id"],
                    ["Irish", "Gaeilge", "ga"],
                    ["Italian", "italiano", "it"],
                    ["Japanese", "日本語", "ja"],
                    ["Javanese", "Jawa", "jw"],
                    ["Kannada", "ಕನ್ನಡ", "kn"],
                    ["Kazakh", "Қазақ", "kk"],
                    ["Khmer", "ភាសាខ្មែរ", "km"],
                    ["Korean", "한국어", "ko"],
                    ["Kurdish", "Kurdî", "ku"],
                    ["Kyrgyz", "Кыргызча", "ky"],
                    ["Lao", "ລາວ", "lo"],
                    ["Latin", "Latinae", "la"],
                    ["Latvian", "Latvijas", "lv"],
                    ["Lithuanian", "Lietuvos", "lt"],
                    ["Luxembourgish", "lëtzebuergesch", "lb"],
                    ["Macedonian", "Македонски", "mk"],
                    ["Malagasy", "Malagasy", "mg"],
                    ["Malay", "Malay", "ms"],
                    ["Malayalam", "മലയാളം", "ml"],
                    ["Maltese", "Malti", "mt"],
                    ["Maori", "Maori", "mi"],
                    ["Marathi", "मराठी", "mr"],
                    ["Mongolian", "Монгол", "mn"],
                    ["Myanmar", "မြန်မာ", "my"],
                    ["Nepali", "नेपाली", "ne"],
                    ["Norwegian", "norsk", "no"],
                    ["Nyanja", "madambwe", "ny"],
                    ["Pashto", "پښتو", "ps"],
                    ["Persian", "فارسی", "fa"],
                    ["Polish", "Polskie", "pl"],
                    ["Portuguese", "português", "pt"],
                    ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
                    ["Romanian", "Română", "ro"],
                    ["Russian", "русский", "ru"],
                    ["Samoan", "Samoa", "sm"],
                    ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
                    ["Serbian", "Српски", "sr"],
                    ["Sesotho", "Sesotho", "st"],
                    ["Shona", "Shona", "sn"],
                    ["Sindhi", "سنڌي", "sd"],
                    ["Sinhala", "සිංහල", "si"],
                    ["Slovak", "slovenský", "sk"],
                    ["Slovenian", "slovenski", "sl"],
                    ["Somali", "Soomaali", "so"],
                    ["Spanish", "Español", "es"],
                    ["Sundanese", "Sunda", "su"],
                    ["Swahili", "Kiswahili", "sw"],
                    ["Swedish", "svenska", "sv"],
                    ["Tajik", "Тоҷикистон", "tg"],
                    ["Tamil", "தமிழ்", "ta"],
                    ["Telugu", "తెలుగు", "te"],
                    ["Thai", "ไทย", "th"],
                    ["Turkish", "Türk", "tr"],
                    ["Ukrainian", "український", "uk"],
                    ["Urdu", "اردو", "ur"],
                    ["Uzbek", "O'zbekiston", "uz"],
                    ["Vietnamese", "Tiếng Việt", "vi"],
                    ["Welsh", "Cymraeg", "cy"],
                    ["Western Frisian", "Western Frysk", "fy"],
                    ["Xhosa", "isiXhosa", "xh"],
                    ["Yiddish", "ייִדיש", "yi"],
                    ["Yoruba", "yorùbá", "yo"],
                    ["Zulu", "Zulu", "zu"]
                ],
                "showAccount": true,
                "accountMenuText": "<SWCtrl controlname="Custom" props="Name:userOptionsDropdownText" />",
                "usePageListNavigation": true,
                "extraMenuOptions": additionalItems,
                "siteID": "[$siteID$]",
                "allLoaded": function(){}
            });
        },

		"ViewFullSite": function() {
			// VIEW FULL SITE BUTTON ON MOBILE
			if(this.ShowFullSiteButton) {
				$("#gb-page").addClass("using-full-site-btn");

				$("body").viewFullSite({
					"breakPoint"		: 639, // SYSTEM BREAK POINTS - 1023, 767, 639, 479
					"buttonContainer"	: "body",
					"useDefaultCSS"		: "yes",
					"fixedPosition"		: "bottom" // IF USING DEFAULT STYLES - TOP, BOTTOM
				});
			}
		},

        "AppAccordion": function() {
            $(".sp-column.one").csAppAccordion({
                "accordionBreakpoints" : [768, 640, 480, 320]
            });
        },

        "Search": function() {
        	// FOR SCOPE
            var _this = this;

			$("#gb-search-form").submit(function(e){
				e.preventDefault();

				if($("#gb-search-input").val() != "Search...") {
                	if($("#gb-search-input").val() != "") {
						window.location.href = "/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=" + $("#gb-search-input").val();
                    }
                }
			});

			$("#gb-search-input").focus(function() {
				if($(this).val() == "Search...") {
					$(this).val("");
				}
			}).blur(function() {
				if($(this).val() == "") {
					$(this).val("Search...");
				}
			});
        },

        "AllyClick": function(event) {
            if(event.type == "click") {
                return true;
            } else if(event.type == "keydown") {
                if(event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter) {
                    return true;
                }
            } else {
                return false;
            }
        },

        "GetBreakPoint": function() {
            return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");/*"*/
        }
    };
